import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import { GerinusService } from 'src/lib/gerinus.service';

@Injectable()
export class PagamentoService extends GerinusService {
 
    constructor(
        public http: HttpClient
    ) {
        super(http);
        this.entityName = "Modalidade_Pagamento";
        this.companyField = "MDP_CodigoEmpresa";
        this.idField = "MDP_CodigoModalidade";
    }
}