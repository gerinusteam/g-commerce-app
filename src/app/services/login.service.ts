import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';
// import swal from 'sweetalert2';
import { Router } from '@angular/router';
import { GerinusService } from 'src/lib/gerinus.service';
import { GlobalService } from 'src/lib/global.service';

@Injectable()
export class LoginService extends GerinusService {

    constructor(
        public http: HttpClient,
        public router: Router,
        public global: GlobalService
    ) {
        super(http);
    }

    public authentication(username, password) {
        let data = {
            username: username,
            password: password
        };
        this.global.startLoading('Autenticando usuário');

        this.http.post(this.apiURL() + '/login/auth/', data, this.headers()).subscribe(
            (response: any) => {
                console.log(response)
                if (response.result) {
                    let user = response.data;
                    localStorage.setItem("user", JSON.stringify(user));
                    this.router.navigate(["/home"]);
                    this.global.showMessage({
                        message: 'Usuário autenticado com sucesso'
                    });
                }
                else {
                    this.global.showMessage({
                        message: response.msg,
                        title: "Erro!"
                    })
                }
                this.global.stopLoading();
            },
            error => {
                this.global.stopLoading();
                console.error('Login ->', error)
                this.global.showMessage({
                    header:"Ops!",
                    message:'Ocorreu um erro ao processar sua solicitação'
                });
            });

    }

    public logout() {
        localStorage.removeItem("user");
        localStorage.clear();
        this.router.navigate(["/login"]);
    }

}