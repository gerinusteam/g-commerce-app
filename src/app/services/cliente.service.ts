import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import { GerinusService } from 'src/lib/gerinus.service';

@Injectable()
export class ClienteService extends GerinusService {
 
    constructor(
        public http: HttpClient
    ) {
        super(http);
        this.entityName = "Cliente";
        this.idField = "CL_CodigoCliente";
    }
}