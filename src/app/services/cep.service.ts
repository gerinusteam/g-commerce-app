import { Injectable } from '@angular/core';
import { HttpClient, HttpHeaders } from '@angular/common/http';
// import swal from 'sweetalert2';
import { Router } from '@angular/router';

@Injectable()
export  class CepService {
    constructor(private http: HttpClient) {
        
    }

    public get(cep) {
        var headers = new HttpHeaders({
            "Content-Type": "application/json",
            "Accept": "application/json"
        });

        return this.http.get(`https://viacep.com.br/ws/${cep}/json/`, {headers});
    }
}