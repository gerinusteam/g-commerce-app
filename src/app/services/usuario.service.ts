import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import { GerinusService } from 'src/lib/gerinus.service';

@Injectable()
export class UsuarioService extends GerinusService {

    constructor(
        public http: HttpClient
    ) {
        super(http);
        this.entityName = "Usuario";
        this.idField = "USR_CodigoUsuario";
        this.companyField = "USR_CodigoEmpresa";
        this.model = "Usuario_model";
        this.orderBy = "USR_Nome";        
    } 

}    