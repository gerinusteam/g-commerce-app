import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import { GerinusService } from 'src/lib/gerinus.service';

@Injectable()
export class VendedorService extends GerinusService {
 
    constructor(
        public http: HttpClient
    ) {
        super(http);
        this.entityName = "Vendedor";
        this.idField = "VND_CodigoVendedor";
        this.model = "Vendedor_model";
    }
}