import { Component } from '@angular/core';
import { LoginService } from '../services/login.service';

@Component({
    selector: 'app-home',
    templateUrl: 'home.page.html',
    styleUrls: ['home.page.scss'],
    providers: [ LoginService ]
})
export class HomePage {

    constructor(
        public login: LoginService
    ) {

    }

}
