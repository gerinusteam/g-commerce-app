import { Component, OnInit } from '@angular/core';
import { GlobalService } from 'src/lib/global.service';
import { GerinusService } from 'src/lib/gerinus.service';
import { GerinusBD } from 'src/lib/gerinus-bd.component';
import { NavController } from '@ionic/angular';

@Component({
  selector: 'app-order',
  templateUrl: './order.page.html',
  styleUrls: ['./order.page.scss'],
})
export class OrderPage extends GerinusBD {

  constructor(
    public global: GlobalService,
    public service: GerinusService,
    public navCtrl: NavController
  ) { 
    super(global, service, navCtrl);
    this.service.entityName = "Pedido";
    this.service.idField = "PED_CodigoPedido";
    this.service.orderBy = "PED_CodigoPedido DESC"; 
    this.afterCreate();
    
  }

  ngOnInit() {
    
  }

}
