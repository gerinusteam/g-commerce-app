import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { FormsModule } from '@angular/forms';
import { IonicModule } from '@ionic/angular'
import { AddPage } from './add.page';
import { BrMaskerModule } from 'br-mask';
import { AddPageRoutingModule } from './add-routing,module';








@NgModule({
  imports: [
    CommonModule,
    FormsModule,
    IonicModule,
    AddPageRoutingModule,
    BrMaskerModule
  ],
  declarations: [AddPage]
})
export class AddPageModule {}
