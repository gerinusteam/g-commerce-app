import { Component, OnInit } from '@angular/core';
import { GlobalService } from 'src/lib/global.service';
import { GerinusService } from 'src/lib/gerinus.service';
import { GerinusBD } from 'src/lib/gerinus-bd.component';
import { ActivatedRoute } from '@angular/router';
import { CepService } from 'src/app/services/cep.service';
import { NavController } from '@ionic/angular';
import { ClienteService } from 'src/app/services/cliente.service';
import { PagamentoService } from 'src/app/services/pagamento.service';
import { EmpresaService } from 'src/app/services/empresa.service';
import { VendedorService } from 'src/app/services/vendedor.service';
import { UsuarioService } from 'src/app/services/usuario.service';



@Component({
    selector: 'app-add',
    templateUrl: './add.page.html',
    styleUrls: ['./add.page.scss'],
    providers: [CepService, PagamentoService, EmpresaService, VendedorService, UsuarioService, ClienteService]
})
export class AddPage extends GerinusBD {

    public id;
    public title;
    public client;
    public modalidade;
    public empresa;
    public tabela;
    public detalhe = "";
    public usuario;
    public vendedor;
    public entity = {};


    constructor(
        public global: GlobalService,
        public service: GerinusService,
        public route: ActivatedRoute,
        public cepService: CepService,
        public cliente: ClienteService,
        public pagamento: PagamentoService,
        public empresaService: EmpresaService,
        public vendedorService: VendedorService,
        public usuarioService: UsuarioService,
        public navCtrl: NavController
    ) {

        super(global, service, navCtrl);
        this.service.entityName = "Pedido";
        this.service.idField = "PED_CodigoPedido";

        console.log('Order Add');
    }


    ngOnInit() {
        this.cliente.listAll().subscribe((response: any) => {
            this.client = response.data;
        });

        this.pagamento.listAll().subscribe((response: any) => {
            this.modalidade = response.data;
        });

        this.empresaService.listAll().subscribe((response: any) => {
            this.empresa = response.data;
        });

        this.usuarioService.listAll().subscribe((response: any) => {
            this.usuario = response.data;
        });

        this.vendedorService.listAll().subscribe((response: any) => {
            this.vendedor = response.data;
        }); 

        if (this.id) {
            this.edit(this.id);
          }
          else {
            this.title = 'Novo Pedido';
          }
    }

}