import { Component } from '@angular/core';
import { GlobalService } from 'src/lib/global.service';
import { GerinusService } from 'src/lib/gerinus.service';
import { GerinusBD } from 'src/lib/gerinus-bd.component';
import { ActivatedRoute } from '@angular/router';
import { CepService } from 'src/app/services/cep.service';
import { NavController } from '@ionic/angular';



@Component({
  selector: 'app-add',
  templateUrl: './add.page.html',
  styleUrls: ['./add.page.scss'],
  providers: [CepService]
})
export class AddPage extends GerinusBD {

  public id;
  public title;
  public tipoPessoa = '1';


  constructor(
    public global: GlobalService,
    public service: GerinusService,
    public route: ActivatedRoute,
    public cepService: CepService,
    public navCtrl:NavController
  ) {

    super(global, service, navCtrl);
    this.service.entityName = "Cliente";
    this.service.idField = "CL_CodigoCliente";

    this.route.params.subscribe(param => {
      if (param.id) {
        this.id = param.id;
        if (this.entity && this.entity.CL_CNPJ.length > 0) {
          this.changeTipoPessoa();
        }

        this.changeTipoPessoa();
      }
      else {
        this.entity = {};
      }
    })

  }

  ngOnInit() {
    if (this.id) {
      this.edit(this.id);
    }
    else {
      this.title = 'Novo Cliente';
    }
  }

  changeTipoPessoa() {
    if (this.tipoPessoa == '1') {
      this.tipoPessoa = '2';
    }
    else {
      this.tipoPessoa = '1';
    }
  }

  public searchCep(cep) {
    if (cep.length >= 9) {

      this.cepService.get(cep).subscribe((response: any) => {
        if (!response.erro) {
          this.entity.CL_Cidade = response.localidade;
          this.entity.CL_Bairro = response.bairro;
          this.entity.CL_UF = response.uf;
          this.entity.CL_Endereco = response.logradouro;
        }
        else {
          this.global.showMessage({
            header: 'Erro',
            message: 'CEP inválido, tente novamente'
          });
        }

        console.log(response);
      },
        error => {
          this.global.showMessage({
            header: 'Ops!',
            message: 'Ocorreu um erro ao processar sua requisição'
          });
        })
    }
  }

  public emailExists() {

    let params = `(CL_EMail = '${this.entity.CL_EMail}')`;

    this.global.startLoading('Verificando dados');

    this.service.listAll(params).subscribe(
      (response: any) => {
        if (response.data.length > 0) {
          this.global.showMessage({
            header: 'Atenção!',
            message: 'Este email já está cadastrado'
          });
          
        }
        this.global.stopLoading();
      },
      error => {
        this.global.stopLoading();
        this.global.showMessage({
          header: 'Ops!',
          message: 'Ocorreu um erro ao processar sua requisição'
        });
      })
  }

}
