import { Component, OnInit } from '@angular/core';
import { GlobalService } from 'src/lib/global.service';
import { GerinusService } from 'src/lib/gerinus.service';
import { GerinusBD } from 'src/lib/gerinus-bd.component';
import { NavController } from '@ionic/angular';

@Component({
  selector: 'app-clients',
  templateUrl: './clients.page.html',
  styleUrls: ['./clients.page.scss'],
  providers:[]
})
export class ClientsPage extends GerinusBD {

  constructor(
    public global: GlobalService,
    public service: GerinusService,
    public navCtrl: NavController
  ) { 
    super(global, service, navCtrl);
    this.service.entityName = "Cliente";
    this.service.idField = "CL_CodigoCliente";
    this.service.orderBy = "CL_CodigoCliente DESC";
    this.afterCreate();
  }

  ngOnInit() {
    
  }

}
