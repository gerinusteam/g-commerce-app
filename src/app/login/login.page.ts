import { Component, OnInit } from "@angular/core";
import { LoginService } from "../services/login.service";
import { GerinusBD } from "src/lib/gerinus-bd.component";
import { GlobalService } from "src/lib/global.service";
import { Router } from '@angular/router';

@Component({
  selector: "app-login",
  templateUrl: "./login.page.html",
  styleUrls: ["./login.page.scss"],
  providers: [LoginService]
})
export class LoginPage extends GerinusBD {
  public email;
  public password;

  constructor(
    public global: GlobalService, 
    public service: LoginService,
    public router: Router,
  ) {
    super(global, service, null);
    
    if (this.global.user()) {
      this.router.navigate(['/home']);
    }
    
  }

  public authentication() {
    this.service.authentication(this.email, this.password);
  }
}
