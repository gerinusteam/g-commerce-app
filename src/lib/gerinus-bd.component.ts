import { GerinusService } from './gerinus.service';
import { GlobalService } from './global.service';

import $ from 'jquery';
import { Location } from '@angular/common';
import { NavController } from '@ionic/angular';




export class GerinusBD  {
    
    public editing = false;

    public useFormData = false;

    public entity = null;
    public list = null;
    public pagination = null;   

    constructor(
        public global: GlobalService,
        public service: GerinusService,
        public navCtrl: NavController
    ) {
        
    }

    public afterCreate() {
        this.listAll();
    }

    public defaults() {
        return {}
    }

    public listCondition() {
        return null;
    }

    public canSave() {
        let invalids = $('.ng-invalid');
        let message = '';
        for (let i = 0; i < invalids.length; i++) {  
            if ($(invalids[i]).attr('placeholder')) {
                if (message != '') message = message + '<br>'; 
                message = message + $(invalids[i]).attr('placeholder');                
            }            
        }
        return {
            ok: ($('.ng-invalid').addClass('ng-touched').length <= 0),
            message: message
        }
    }

    public save(afterSave=null) {
        let canSave = this.canSave();
        if (canSave.ok) {
            this.global.startLoading("Salvando...");
            this.service.save(this.entity,this.useFormData).subscribe((response:any) => { 
                console.log('Response Save ->',response);           
                this.listAll();
                if (response.ok) {
                    this.editing = false;
                    this.global.stopLoading();
                    this.global.showMessage({
                        header: "Salvar!",
                        message: response.message,
                        type: response.message_type,
                    });

                    this.listAll();

                    if (afterSave) {
                        this.navCtrl.back();
                        // this.navCtrl.back();
                        afterSave();
                    }

                    // window.history.back();
                } 
                else {
                    this.global.stopLoading();
                    this.global.showMessage({
                        header: "Erro!",
                        message: response.message,
                        type: response.message_type,
                    });    
                }
            });
        } 
        else {
            this.global.showMessage({
                header:'Erro',
                message:canSave.message,
                type:'error'
            });
        }
    }

    public add() {
        this.editing = true;
        this.entity = this.defaults();
    }

    public edit(id) {
        this.editing = true;
        this.service.getEntity(id).subscribe((response:any) => {
            this.entity = response.entity;
        });
    }

    public async delete(id) {
        const alert = await this.global.alert.create({
            header: "Remover?",
            message:"Você tem certeza que deseja remover este registro?",
            buttons: [
                {
                  text: 'Não',
                  role: 'cancel',
                  cssClass: 'secondary',
                }, {
                  text: 'Sim',
                  handler: () => {
                    this.global.startLoading("Removendo Registro...");
                    setTimeout(() => {
                        this.service.delete(id).subscribe((response:any) => {
                            if (response.ok) {
                                this.global.stopLoading();
                                this.global.showMessage({
                                    header: "Removido!",
                                    text: response.message,
                                    type: response.message_type,
                                });

                                window.history.back();

                            } 
                            else {
                                this.global.stopLoading();
                                this.global.showMessage({
                                    header: "Erro!",
                                    text: response.message,
                                    type: response.message_type,
                                });    
                            }  
                            this.listAll();
                        });   
                    }, 100);   
                  }
                }
            ]
        });

        alert.present();
    }

    public paginateMenu() {
        let result = [];
        if (this.pagination) {
            let page = 1;
            while (page <= this.pagination.last) {
                result.push(page);
                page++;
                if ((page == 4) && (this.pagination.last > 6)) {
                    result.push(0);
                    page = this.pagination.last - 2;
                }
            }
        }
        return result;
    }

    public listAll(page = 1) {
        if (this.service.entityName) {
            this.list = null;
            this.pagination = null;
            this.list = null;
            setTimeout(() => {
                this.service.listAll(this.listCondition(),page).subscribe((response:any) => {
                    if (response && response.data) {
                        console.log(response);
                        this.list = response.data;
                        if (response.pagination) {
                            this.pagination = response.pagination;
                        }
                    }
                    else {
                        this.list = response;
                    }
                });                
            }, 100);

        }
    }


}