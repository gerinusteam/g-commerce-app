import { Injectable } from '@angular/core';
import { environment } from 'src/environments/environment';
import { LoadingController, AlertController } from '@ionic/angular';


@Injectable()
export class GlobalService {

    public loader;
    
    constructor(
        public loading: LoadingController,
        public alert: AlertController,
    ){}


    public user() {
        let user:any = localStorage.getItem("user");
        if (user) {
            user = JSON.parse(user);            
        }
        return user;
    } 

    public apiURL(URL=null) {
        if (URL) {
            return environment.apiURL + '/' + URL;
        }
        return environment.apiURL;
    }

    public async startLoading(message='Carregando') {
       if (!this.loader) {
        this.loader = await this.loading.create({
            message:message
        });
        
        this.loader.present();
       }
       else {
           this.loader.setMessage(message);
       }
        

       
    }

    public stopLoading() {
       this.loader.dismiss();
       this.loader = null;
    }

    public async showMessage(options) {
        const alert = await this.alert.create({
            ...options,
            buttons: ['OK']
          });
      
        await alert.present();
    }
    
}