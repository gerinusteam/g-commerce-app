import { HttpClient } from '@angular/common/http';
import { Injectable } from '@angular/core';
import { environment } from 'src/environments/environment';

@Injectable()
export class GerinusService {
    
    public controller = "generic";
    public model = "Generic_model";
    public entityName = null;
    public idField = null; 
    public companyField = null;
    public companyID = null;
    public orderBy = null;
    public cnpj = null;

    constructor(
        public http: HttpClient
    ) {
        
    }

    public apiURL() {
        return environment.apiURL;
    }

    public headers(useFormData=false) {
        let entityData:any = {
            'model' : this.model,
            'entityName' : this.entityName,
            'idField' : this.idField,
            'orderBy' : this.orderBy,
        }

        let user = localStorage.getItem("user");
        if (user) {
            entityData.Gerinus = btoa(user);
        }

        let header = {
            'Content-Type': 'json',
            'EntityData' : JSON.stringify(entityData)
        } 
        
        if (useFormData) {
            delete(header['Content-Type']); 
        }
        
        return {
            headers: header
        }

    }

    public getEntity(id) {
        return this.http.get(this.apiURL() + "/" + this.controller + "/edit/" + id, this.headers());
    }

    public save(entity, useFormData=false) {
        let formData = null;
        if (useFormData) {
            formData = new FormData();
            let keys = Object.keys(entity);
            
            for (let key of keys) { 
                let foto = key.substring(4,8);   
                console.log(foto);             
                if (foto == "Foto") {
                    console.log('key 0 = ',key); 
                    formData.append(key,entity[key]);   
                }
                else {                                                      
                    if (typeof entity[key] == 'object') {         
                        console.log('key 1 = ',key,entity[key]);           
                        formData.append(key,JSON.stringify(entity[key]));
                    }
                    else {
                        console.log('key 2 = ',key,entity[key]);
                        formData.append(key,entity[key]);
                    }
                }
                
            }
        }
        if (formData) {
            console.log('formData = ',formData);
            return this.http.post(this.apiURL() + "/" + this.controller + "/save/" + entity[this.idField],formData,this.headers(true));
        }
        else {
            return this.http.post(this.apiURL() + "/" + this.controller + "/save/" + entity[this.idField],entity,this.headers());
        }
        
    }

    public delete(id) {
        return this.http.get(this.apiURL() + "/" + this.controller + "/remove/" + id, this.headers());
    }

    public listAll(condition=null,page=1) {
        let where = {}
        let pageQry = "";

        if (condition) {
            where = {
                where: condition
            }
        }
        if (page != 1) {
            pageQry = "/" + page;
        }

        return this.http.post(this.apiURL() + "/" + this.controller + "/listAll" + pageQry, where, this.headers());
    }

    
}